DATABASE_URI = 'sqlite:///example.db'

default_segments = [
    'Junior Backend',
    'Mid Backend',
    'Senior Backend',
    'Junior Frontend',
    'Mid Frontend',
    'Senior Frontend',
    'Junior Data Engineer',
    'Mid Data Engineer',
    'Senior Data Engineer'
]
