from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.exc import NoResultFound

import os

from config import DATABASE_URI
from models import Model, Article, User, Tag, Segment, TagBonus

engine = create_engine(os.environ.get('DATABASE_URI', DATABASE_URI))
Model.metadata.bind = engine
Session = sessionmaker(bind=engine)

def _eval_rank_point(article, segments):
    point = len(article.likers)

    for tag in article.tags:
        for bonus in tag.bonuses:
            if bonus.segment in segments:
                point += bonus.point

    return point

def _ranking(articles, segments):
    ranking = list(zip(map(lambda a: _eval_rank_point(a, segments), articles), articles))
    ranking.sort(key = lambda e: e[0], reverse=True)
    return ranking

def refresh_tag_bonus():
    session = Session()
    # session.query(TagBonus).delete()
    tags = session.query(Tag).all()
    segments = session.query(Segment).all()

    for tag in tags:
        for segment in segments:
            if session.query(TagBonus).filter_by(tag=tag, segment=segment).count() == 0:
                bonus = TagBonus(tag=tag, segment=segment, point=0)
                session.add(bonus)

    session.commit()
    bonuses = session.query(TagBonus).all()

    for bonus in bonuses:
        tagged_articles = (session
                           .query(Article)
                           .filter(Article.tags.any(Tag.id == bonus.tag.id))
                           .all())
        if len(tagged_articles) == 0:
            bonus.point = 0
        else:
            likes = 0
            for article in tagged_articles:
                likes += len(list(filter(
                    lambda user: bonus.segment in user.segments, article.likers)))

            bonus.point = likes/len(tagged_articles)
    session.commit()
    session.close()

def fetch_articles(username, tag_names=[]):
    session = Session()
    articles = []
    if tag_names:
        for tag_name in tag_names:
            articles += session.query(Article).filter(Article.tags.any(Tag.name == tag_name)).all()
    else:
        articles += session.query(Article).all()

    segments = session.query(User).filter_by(username = username).one().segments

    ranking = _ranking(articles, segments)

    return [article for (point, article) in ranking]

def add_user(username, segment_names):
    session = Session()
    segments = list(map(lambda name: session.query(Segment).filter_by(name=name).one(), segment_names))
    new_user = User(username=username, segments=segments)
    session.add(new_user)
    session.commit()
    id = new_user.id
    session.close()
    return id

def update_user_segments(username, segment_names):
    session = Session()
    segments = list(map(lambda name: session.query(Segment).filter_by(name=name).one(), segment_names))
    user = session.query(User).filter_by(username=username).one()
    user.segments = segments
    session.commit()
    session.close()

def add_article(title, poster_username, url='', summary='', tag_names=[]):
    session = Session()
    poster = session.query(User).filter_by(username = poster_username).one()
    new_article = Article(
        title = title,
        poster = poster,
        url = url,
        summary = summary)

    tags = []
    for tag_name in tag_names:
        try:
            tag = session.query(Tag).filter_by(name = tag_name).one()
            tags.append(tag)
        except NoResultFound:
            tag = Tag(name = tag_name)
            session.add(tag)
            tags.append(tag)

    new_article.tags = tags
    session.add(new_article)
    session.commit()
    id = new_article.id
    session.close()
    return id

# ... modify article {title, url, summary}

def add_tag(article_id, tag_name):
    session = Session()
    try:
        tag = session.query(Tag).filter_by(name = tag_name).one()
    except NoResultFound:
        tag = Tag(name = tag_name)
        session.add(tag)
    article = session.query(Article).filter_by(id = article_id).one()
    article.tags.append(tag)
    session.commit()
    session.close()

def remove_tag(article_id, tag_name):
    session = Session()
    try:
        tag = session.query(Tag).filter_by(name = tag_name).one()
        article = session.query(Article).filter_by(id = article_id).one()
        article.tags.remove(tag)
        session.commit()
    except NoResultFound:
        pass
    session.close()

def like(article_id, username):
    session = Session()
    article = session.query(Article).filter_by(id = article_id).one()
    user = session.query(User).filter_by(username=username).one()
    if user not in article.likers:
        article.likers.append(user)
    session.commit()
    session.close()

def unlike(article_id, username):
    session = Session()
    article = session.query(Article).filter_by(id = article_id).one()
    user = session.query(User).filter_by(username=username).one()
    if user in article.likers:
        article.likers.remove(user)
    session.commit()
    session.close()
