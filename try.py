import csv
import os

try:
    os.remove('example.db')
except FileNotFoundError:
    pass

from sqlalchemy.exc import IntegrityError
import api

sne = 'Senior Data Engineer'
sb = 'Senior Backend'
mf = 'Mid Frontend'

users_to_create = {
    'editor': sne,
    'simon': sb,
    'hannah': mf,
    'david': sb,
    'jean': sne,
    'bob': mf,
    'scott': sb,
    'nat': sne,
    'daphne': sne,
    'hui': mf
}

for (username, segment) in users_to_create.items():
    try:
        api.add_user(username, [segment])
    except IntegrityError:
        pass # if this username is already exists

with open('articles_source.csv', newline='') as source_file:
    reader = csv.reader(source_file)
    for row in reader:
        [title, tag_string] = row
        tags = tag_string.split(', ')

        api.add_article(title, 'editor', tag_names = tags)

# Selective CRUD operations
api.update_user_segments('simon', [mf])
api.add_tag(50, 'new-tag')
api.remove_tag(50, 'new-tag')

# Let people like random articles
# TODO make it truly random, use random module

for i in range(10,40):
    api.like(i, 'simon')

api.unlike(30, 'simon')

for i in range(50,80):
    api.like(i, 'hannah')

for i in range(30,70):
    api.like(i, 'david')

for i in range(20,70):
    api.like(i, 'bob')

api.refresh_tag_bonus()

for articles in api.fetch_articles('simon'):
    print('{} {}'.format(articles.id, articles.title))
