### Environment Variables

* `DATABASE_URI` Specify which database to connect, default to `'sqlite:///example.db'`

### API reference

* `add_user(username: String, segment_names: List[String]) : Integer`

  Create new user in database, return that user's id

* `update_user_segments(username: String, segment_names: List[String])`

* `add_article(title: String, poster_username: String, url: String = '', summary: String = '', tag_names=[])`

  Create new article in database, return that article's id

* `add_tag(article_id: String, tag_name: String)`

  Add a tag into an article

* `remove_tag(article_id: String, tag_name: String)`

  Remove a tag from an article

* `like(article_id: String, username: String)`

  Let a user like an article

* `unlike(article_id: String, username: String)`

  Remove a like