from sqlalchemy import Column, Table, ForeignKey, UniqueConstraint, Integer, String
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy import create_engine

import os
from config import DATABASE_URI

Model = declarative_base()

article_tag_assoc_table = Table(
    'article_tag_assoc',
    Model.metadata,
    Column('article_id', Integer, ForeignKey('article.id')),
    Column('tag_id', Integer, ForeignKey('tag.id')))

article_liker_assoc_table = Table(
    'article_liker_assoc',
    Model.metadata,
    Column('article_id', Integer, ForeignKey('article.id')),
    Column('liker_id', Integer, ForeignKey('user.id')))

class Article(Model):
    __tablename__ = 'article'
    id = Column(Integer, primary_key=True)
    title = Column(String(250), nullable=False)
    url = Column(String(1000))
    summary = Column(String(1000))

    tags = relationship('Tag',
                        secondary = article_tag_assoc_table,
                        backref = 'articles')

    poster_id = Column(Integer, ForeignKey('user.id'))
    poster = relationship('User',
                          backref = 'articles')

    likers = relationship('User',
                          secondary = article_liker_assoc_table,
                          backref = 'likes')

class Tag(Model):
    __tablename__ = 'tag'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)

user_segment_assoc_table = Table(
    'user_segment_assoc',
    Model.metadata,
    Column('user_id', Integer, ForeignKey('user.id')),
    Column('segment_id', Integer, ForeignKey('segment.id')))

class User(Model):
    __tablename__ = 'user'
    id = Column(Integer, primary_key=True)
    username = Column(String(50), unique=True)
    segments = relationship('Segment',
                            secondary = user_segment_assoc_table,
                            backref = 'users')

class Segment(Model):
    __tablename__ = 'segment'
    id = Column(Integer, primary_key=True)
    name = Column(String(50), unique=True)

# Bonus rank points is manually refreshed on demand by API call
class TagBonus(Model):
    __tablename__ = 'tag_bonus'
    tag_id = Column(Integer, ForeignKey('tag.id'), primary_key=True)
    tag = relationship('Tag', backref = 'bonuses')
    segment_id = Column(Integer, ForeignKey('segment.id'), primary_key=True)
    segment = relationship('Segment')
    point = Column(Integer)

engine = create_engine(os.environ.get('DATABASE_URI', DATABASE_URI))

Model.metadata.create_all(engine)

import init
