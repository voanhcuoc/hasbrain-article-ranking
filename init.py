from models import Segment, Model, engine
from sqlalchemy.orm import sessionmaker

from config import default_segments

Session = sessionmaker(engine)
session = Session()

for segment_name in default_segments:
    if session.query(Segment).filter_by(name = segment_name).count() == 0:
        session.add(Segment(name=segment_name))

session.commit()
session.close()
